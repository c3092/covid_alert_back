package fr.poly.mtp.ig5.iwa.covid_alert_back;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CovidAlertBackApplication {

    public static void main(String[] args) {
        SpringApplication.run(CovidAlertBackApplication.class, args);
    }

}
